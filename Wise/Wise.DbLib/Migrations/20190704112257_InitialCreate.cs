﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wise.DbLib.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Certification",
                columns: table => new
                {
                    id_certification = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code_RNCP = table.Column<string>(unicode: false, maxLength: 6, nullable: true),
                    code_CERTIFINFO = table.Column<string>(unicode: false, maxLength: 6, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Certification", x => x.id_certification);
                });

            migrationBuilder.CreateTable(
                name: "CodeFORMACODE",
                columns: table => new
                {
                    id_code_FORMACODE = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeFORMACODE", x => x.id_code_FORMACODE);
                });

            migrationBuilder.CreateTable(
                name: "CodeNSF",
                columns: table => new
                {
                    id_code_NSF = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 3, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeNSF", x => x.id_code_NSF);
                });

            migrationBuilder.CreateTable(
                name: "CodeROME",
                columns: table => new
                {
                    id_code_ROME = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodeROME", x => x.id_code_ROME);
                });

            migrationBuilder.CreateTable(
                name: "DomaineFormation",
                columns: table => new
                {
                    id_domaine_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DomaineFormation", x => x.id_domaine_formation);
                });

            migrationBuilder.CreateTable(
                name: "EtatRecrutement",
                columns: table => new
                {
                    id_etat_recrutement = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    recrutement_key = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EtatRecrutement", x => x.id_etat_recrutement);
                });

            migrationBuilder.CreateTable(
                name: "Geolocalisation",
                columns: table => new
                {
                    id_geolocalisation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    latitude = table.Column<string>(unicode: false, maxLength: 30, nullable: false),
                    longitude = table.Column<string>(unicode: false, maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Geolocalisation", x => x.id_geolocalisation);
                });

            migrationBuilder.CreateTable(
                name: "Ligne",
                columns: table => new
                {
                    id_ligne = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ligne = table.Column<string>(unicode: false, maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ligne", x => x.id_ligne);
                });

            migrationBuilder.CreateTable(
                name: "ModaliteEnseignement",
                columns: table => new
                {
                    id_modalite_enseignement = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    modalite_key = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModaliteEnseignement", x => x.id_modalite_enseignement);
                });

            migrationBuilder.CreateTable(
                name: "ModulePrerequis",
                columns: table => new
                {
                    id_modules_prerequis = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModulePrerequis", x => x.id_modules_prerequis);
                });

            migrationBuilder.CreateTable(
                name: "Niveaux",
                columns: table => new
                {
                    id_niveaux = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 65, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Niveaux", x => x.id_niveaux);
                });

            migrationBuilder.CreateTable(
                name: "Numtel",
                columns: table => new
                {
                    id_numtel = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    numero = table.Column<string>(unicode: false, maxLength: 25, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Numtel", x => x.id_numtel);
                });

            migrationBuilder.CreateTable(
                name: "ObjectifGeneralFormation",
                columns: table => new
                {
                    id_objectif_general_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 70, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObjectifGeneralFormation", x => x.id_objectif_general_formation);
                });

            migrationBuilder.CreateTable(
                name: "Offre",
                columns: table => new
                {
                    id_offre = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offre", x => x.id_offre);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFinanceur",
                columns: table => new
                {
                    id_organisme_financeur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    code_financeur = table.Column<int>(nullable: false),
                    nb_place_financees = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganismeFinanceur", x => x.id_organisme_financeur);
                });

            migrationBuilder.CreateTable(
                name: "ParcoursDeFormation",
                columns: table => new
                {
                    id_parcours_de_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 35, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParcoursDeFormation", x => x.id_parcours_de_formation);
                });

            migrationBuilder.CreateTable(
                name: "PerimetreRecrutement",
                columns: table => new
                {
                    id_perimetre_recrutement = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    label = table.Column<string>(unicode: false, maxLength: 100, nullable: false),
                    cle = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PerimetreRecrutement", x => x.id_perimetre_recrutement);
                    table.UniqueConstraint("AK_PerimetreRecrutement_cle", x => x.cle);
                });

            migrationBuilder.CreateTable(
                name: "Periode",
                columns: table => new
                {
                    id_periode = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    debut = table.Column<DateTime>(type: "date", nullable: false),
                    fin = table.Column<DateTime>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Periode", x => x.id_periode);
                });

            migrationBuilder.CreateTable(
                name: "Positionnement",
                columns: table => new
                {
                    id_positionnement = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 15, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Positionnement", x => x.id_positionnement);
                });

            migrationBuilder.CreateTable(
                name: "Potentiel",
                columns: table => new
                {
                    id_potentiel = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Potentiel", x => x.id_potentiel);
                });

            migrationBuilder.CreateTable(
                name: "ReferenceModule",
                columns: table => new
                {
                    id_reference_module = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    reference = table.Column<string>(unicode: false, maxLength: 3000, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReferenceModule", x => x.id_reference_module);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    IdRole = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.IdRole);
                });

            migrationBuilder.CreateTable(
                name: "SIRET",
                columns: table => new
                {
                    id_SIRET = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SIRET = table.Column<string>(unicode: false, maxLength: 14, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SIRET", x => x.id_SIRET);
                });

            migrationBuilder.CreateTable(
                name: "SousModule",
                columns: table => new
                {
                    id_sous_module = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SousModule", x => x.id_sous_module);
                });

            migrationBuilder.CreateTable(
                name: "TypeModule",
                columns: table => new
                {
                    id_type_module = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    libele = table.Column<string>(unicode: false, maxLength: 25, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeModule", x => x.id_type_module);
                });

            migrationBuilder.CreateTable(
                name: "UrlAction",
                columns: table => new
                {
                    id_url_action = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UrlAction", x => x.id_url_action);
                });

            migrationBuilder.CreateTable(
                name: "UrlFormation",
                columns: table => new
                {
                    id_url_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UrlFormation", x => x.id_url_formation);
                });

            migrationBuilder.CreateTable(
                name: "UrlWeb",
                columns: table => new
                {
                    id_url_web = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    url = table.Column<string>(unicode: false, maxLength: 400, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UrlWeb", x => x.id_url_web);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    IdUser = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.IdUser);
                });

            migrationBuilder.CreateTable(
                name: "Web",
                columns: table => new
                {
                    id_web = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Web", x => x.id_web);
                });

            migrationBuilder.CreateTable(
                name: "Adresse",
                columns: table => new
                {
                    id_adresse = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    codepostal = table.Column<string>(unicode: false, maxLength: 5, nullable: false),
                    ville = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    departement = table.Column<string>(unicode: false, maxLength: 3, nullable: true),
                    code_INSEE_commune = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    code_INSEE_canton = table.Column<string>(unicode: false, maxLength: 5, nullable: true),
                    region = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    pays = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    id_geolocalisation = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adresse", x => x.id_adresse);
                    table.ForeignKey(
                        name: "fk_id_geolocalisation_adresse",
                        column: x => x.id_geolocalisation,
                        principalTable: "Geolocalisation",
                        principalColumn: "id_geolocalisation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactTel",
                columns: table => new
                {
                    id_contact_tel = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_numtel = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactTel", x => x.id_contact_tel);
                    table.ForeignKey(
                        name: "fk_id_numtel_contact_tel",
                        column: x => x.id_numtel,
                        principalTable: "Numtel",
                        principalColumn: "id_numtel",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Lheo",
                columns: table => new
                {
                    id_lheo = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_offre = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lheo", x => x.id_lheo);
                    table.ForeignKey(
                        name: "fk_id_offre_lheo",
                        column: x => x.id_offre,
                        principalTable: "Offre",
                        principalColumn: "id_offre",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    id_module = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    reference_module = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    id_type_module = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Module", x => x.id_module);
                    table.ForeignKey(
                        name: "fk_id_type_module_module",
                        column: x => x.id_type_module,
                        principalTable: "TypeModule",
                        principalColumn: "id_type_module",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserRole",
                columns: table => new
                {
                    IdUser = table.Column<long>(nullable: false),
                    IdRole = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRole", x => new { x.IdRole, x.IdUser });
                    table.ForeignKey(
                        name: "FK_UserRole_Roles_IdRole",
                        column: x => x.IdRole,
                        principalTable: "Roles",
                        principalColumn: "IdRole",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserRole_Users_IdUser",
                        column: x => x.IdUser,
                        principalTable: "Users",
                        principalColumn: "IdUser",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AdresseInformation",
                columns: table => new
                {
                    id_adresse_information = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_adresse = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdresseInformation", x => x.id_adresse_information);
                    table.ForeignKey(
                        name: "fk_adresse_information_id_adresse",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AdresseInscription",
                columns: table => new
                {
                    id_adresse_inscription = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_adresse = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AdresseInscription", x => x.id_adresse_inscription);
                    table.ForeignKey(
                        name: "fk_id_adresse_adresse_inscription",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Coordonnee",
                columns: table => new
                {
                    id_coordonnee = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    civilite = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    nom = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    prenom = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    id_adresse = table.Column<int>(nullable: true),
                    id_telfixe = table.Column<int>(nullable: false),
                    id_portable = table.Column<int>(nullable: false),
                    id_fax = table.Column<int>(nullable: false),
                    courriel = table.Column<string>(unicode: false, maxLength: 160, nullable: true),
                    id_web = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coordonnee", x => x.id_coordonnee);
                    table.ForeignKey(
                        name: "fk_id_adresse_coordonnee",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_fax_coordonnee",
                        column: x => x.id_fax,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_portable_coordonnee",
                        column: x => x.id_portable,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_telfixe_coordonnee",
                        column: x => x.id_telfixe,
                        principalTable: "ContactTel",
                        principalColumn: "id_contact_tel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_web_coordonnee",
                        column: x => x.id_web,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactFormateur",
                columns: table => new
                {
                    id_contact_formateur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactFormateur", x => x.id_contact_formateur);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_formateur",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactFormation",
                columns: table => new
                {
                    id_contact_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactFormation", x => x.id_contact_formation);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_formation",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactOrganisme",
                columns: table => new
                {
                    id_contact_organisme = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactOrganisme", x => x.id_contact_organisme);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_contact_organisme",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CoordonneeOrganisme",
                columns: table => new
                {
                    id_coordonnee_organisme = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoordonneeOrganisme", x => x.id_coordonnee_organisme);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_coordonnee_organisme_coordonne",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LieuDeFormation",
                columns: table => new
                {
                    id_lieu_de_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_coordonnee = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LieuDeFormation", x => x.id_lieu_de_formation);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_lieu_de_formation",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFormateur",
                columns: table => new
                {
                    id_organisme_formateur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_siret_formation = table.Column<int>(nullable: false),
                    raison_sociale_formateur = table.Column<string>(unicode: false, maxLength: 200, nullable: false),
                    id_contact_formateur = table.Column<int>(nullable: true),
                    id_potentiel = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganismeFormateur", x => x.id_organisme_formateur);
                    table.ForeignKey(
                        name: "fk_id_contact_formateur_organisme_formateur",
                        column: x => x.id_contact_formateur,
                        principalTable: "ContactFormateur",
                        principalColumn: "id_contact_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_organisme_formateur",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_organisme_formateur",
                        column: x => x.id_siret_formation,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrganismeFormationResponsable",
                columns: table => new
                {
                    id_organisme_formation_responsable = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    numero_activite = table.Column<string>(unicode: false, maxLength: 11, nullable: false),
                    id_SIRET = table.Column<int>(nullable: false),
                    nom_organisme = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    raison_sociale = table.Column<string>(unicode: false, maxLength: 250, nullable: false),
                    id_coordonnee_organisme = table.Column<int>(nullable: false),
                    id_contact_organisme = table.Column<int>(nullable: false),
                    renseignements_specifiques = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_potentiel = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrganismeFormationResponsable", x => x.id_organisme_formation_responsable);
                    table.ForeignKey(
                        name: "fk_id_contact_organisme_organisme_formation_responsable",
                        column: x => x.id_contact_organisme,
                        principalTable: "ContactOrganisme",
                        principalColumn: "id_contact_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_organisme_organisme_formation_responsable",
                        column: x => x.id_coordonnee_organisme,
                        principalTable: "CoordonneeOrganisme",
                        principalColumn: "id_coordonnee_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_organisme_formation_responsable",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_SIRET_organisme_formation_responsable",
                        column: x => x.id_SIRET,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "_Action",
                columns: table => new
                {
                    id_action = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    rythme_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    info_public_vise = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    niveau_entree_obligatoire = table.Column<bool>(nullable: false),
                    modalite_alternance = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_modalite_enseignement = table.Column<int>(nullable: false),
                    condition_specifiques = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    prise_en_charge_frais_possible = table.Column<bool>(nullable: false),
                    id_lieu_de_formation = table.Column<int>(nullable: false),
                    modalites_entrees_sorties = table.Column<bool>(nullable: false),
                    id_url_action = table.Column<int>(nullable: true),
                    id_adresse_information = table.Column<int>(nullable: true),
                    restauration = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    hebergement = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    transport = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    acces_handicape = table.Column<string>(unicode: false, maxLength: 250, nullable: true),
                    lange_formation = table.Column<string>(unicode: false, maxLength: 2, nullable: true),
                    modalite_recrutement = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    modalite_pedagogiques = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    frais_restant = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    code_perimetre_recrutement = table.Column<int>(nullable: true),
                    info_perimetre_recrutement = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    prix_horaire_ttc = table.Column<decimal>(nullable: true),
                    prix_total_ttc = table.Column<decimal>(nullable: true),
                    duree_indicative = table.Column<string>(unicode: false, maxLength: 200, nullable: true),
                    nombre_heures_centre = table.Column<int>(nullable: true),
                    nombre_heures_enterprise = table.Column<int>(nullable: true),
                    nombre_heures_total = table.Column<int>(nullable: true),
                    detail_condition_prise_en_charge = table.Column<string>(unicode: false, maxLength: 600, nullable: true),
                    conventionnement = table.Column<bool>(nullable: true),
                    duree_conventionee = table.Column<int>(nullable: true),
                    id_organisme_formateur = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Action", x => x.id_action);
                    table.ForeignKey(
                        name: "fk_code_perimetre_recrutement_action",
                        column: x => x.code_perimetre_recrutement,
                        principalTable: "PerimetreRecrutement",
                        principalColumn: "cle",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_action_id_adresse_information",
                        column: x => x.id_adresse_information,
                        principalTable: "AdresseInformation",
                        principalColumn: "id_adresse_information",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_lieu_de_formation_action",
                        column: x => x.id_lieu_de_formation,
                        principalTable: "LieuDeFormation",
                        principalColumn: "id_lieu_de_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_modalite_enseignement_action",
                        column: x => x.id_modalite_enseignement,
                        principalTable: "ModaliteEnseignement",
                        principalColumn: "id_modalite_enseignement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formateur_action",
                        column: x => x.id_organisme_formateur,
                        principalTable: "OrganismeFormateur",
                        principalColumn: "id_organisme_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_action_action",
                        column: x => x.id_url_action,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Formation",
                columns: table => new
                {
                    id_formation = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_domaine_formation = table.Column<int>(nullable: false),
                    intitule_formation = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    objectif_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    resultats_attendus = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    contenu_formation = table.Column<string>(unicode: false, maxLength: 3000, nullable: false),
                    certifiante = table.Column<bool>(nullable: false),
                    id_contact_formation = table.Column<int>(nullable: false),
                    id_parcours_de_formation = table.Column<int>(nullable: false),
                    id_niveaux_entree = table.Column<int>(nullable: false),
                    id_objectif_general_formation = table.Column<int>(nullable: true),
                    id_code_niveau_sortie = table.Column<int>(nullable: true),
                    id_url_formation = table.Column<int>(nullable: true),
                    id_organisme_formation_responsable = table.Column<int>(nullable: false),
                    identifiant_module = table.Column<string>(unicode: false, maxLength: 3000, nullable: true),
                    id_positionnement = table.Column<int>(nullable: true),
                    id_sous_module = table.Column<int>(nullable: true),
                    id_modules_prerequis = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Formation", x => x.id_formation);
                    table.ForeignKey(
                        name: "fk_id_code_niveau_sortie_formation",
                        column: x => x.id_code_niveau_sortie,
                        principalTable: "Niveaux",
                        principalColumn: "id_niveaux",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formation_formation",
                        column: x => x.id_contact_formation,
                        principalTable: "ContactFormation",
                        principalColumn: "id_contact_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_formation",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_modules_prerequis_formation",
                        column: x => x.id_modules_prerequis,
                        principalTable: "ModulePrerequis",
                        principalColumn: "id_modules_prerequis",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_niveaux_entree_formation",
                        column: x => x.id_niveaux_entree,
                        principalTable: "Niveaux",
                        principalColumn: "id_niveaux",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_objectif_general_formation",
                        column: x => x.id_objectif_general_formation,
                        principalTable: "ObjectifGeneralFormation",
                        principalColumn: "id_objectif_general_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formation_responsable_formation",
                        column: x => x.id_organisme_formation_responsable,
                        principalTable: "OrganismeFormationResponsable",
                        principalColumn: "id_organisme_formation_responsable",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_parcours_de_formation_formation",
                        column: x => x.id_parcours_de_formation,
                        principalTable: "ParcoursDeFormation",
                        principalColumn: "id_parcours_de_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_positionnement_formation",
                        column: x => x.id_positionnement,
                        principalTable: "Positionnement",
                        principalColumn: "id_positionnement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_sous_module_formation",
                        column: x => x.id_sous_module,
                        principalTable: "SousModule",
                        principalColumn: "id_sous_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_formation_formation",
                        column: x => x.id_url_formation,
                        principalTable: "UrlFormation",
                        principalColumn: "id_url_formation",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "_Session",
                columns: table => new
                {
                    id_session = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    id_periode = table.Column<int>(nullable: false),
                    id_adresse_inscription = table.Column<int>(nullable: false),
                    modalite_inscription = table.Column<string>(unicode: false, maxLength: 255, nullable: false),
                    id_periode_inscription = table.Column<int>(nullable: true),
                    id_etat_recrutement = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK__Session", x => x.id_session);
                    table.ForeignKey(
                        name: "fk_id_action_session",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_inscription_session",
                        column: x => x.id_adresse_inscription,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_etat_recrutement_session",
                        column: x => x.id_etat_recrutement,
                        principalTable: "EtatRecrutement",
                        principalColumn: "id_etat_recrutement",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_session",
                        column: x => x.id_periode,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_inscription_session",
                        column: x => x.id_periode_inscription,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ActionOrganismeFinanceur",
                columns: table => new
                {
                    id_action_organisme_financeur = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    id_organisme_financeur = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionOrganismeFinanceur", x => x.id_action_organisme_financeur);
                    table.ForeignKey(
                        name: "fk_action_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_action_id_organisme_financeur",
                        column: x => x.id_organisme_financeur,
                        principalTable: "OrganismeFinanceur",
                        principalColumn: "id_organisme_financeur",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CodePublicVise",
                columns: table => new
                {
                    id_code_public_vise = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    code = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CodePublicVise", x => x.id_code_public_vise);
                    table.ForeignKey(
                        name: "fk_code_public_vise_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DateInformation",
                columns: table => new
                {
                    id_date_information = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    date_information = table.Column<DateTime>(type: "date", nullable: false),
                    id_action = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DateInformation", x => x.id_date_information);
                    table.ForeignKey(
                        name: "fk_date_information_id_action",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModalitePedagogique",
                columns: table => new
                {
                    id_modalite_pedagogique = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_action = table.Column<int>(nullable: false),
                    label = table.Column<string>(unicode: false, maxLength: 5, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModalitePedagogique", x => x.id_modalite_pedagogique);
                    table.ForeignKey(
                        name: "fk_id_action_modalite_information",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Extra",
                columns: table => new
                {
                    id_extra = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    id_lheo = table.Column<int>(nullable: true),
                    id_offre = table.Column<int>(nullable: true),
                    id_formation = table.Column<int>(nullable: true),
                    id_domaine_formation = table.Column<int>(nullable: true),
                    id_contact_formation = table.Column<int>(nullable: true),
                    id_coordonnee = table.Column<int>(nullable: true),
                    id_adresse = table.Column<int>(nullable: true),
                    id_geolocalisation = table.Column<int>(nullable: true),
                    id_web = table.Column<int>(nullable: true),
                    id_certification = table.Column<int>(nullable: true),
                    id_sous_module = table.Column<int>(nullable: true),
                    id_module = table.Column<int>(nullable: true),
                    id_modules_prerequis = table.Column<int>(nullable: true),
                    id_organisme_formation_responsable = table.Column<int>(nullable: true),
                    id_SIRET = table.Column<int>(nullable: true),
                    id_coordonnee_organisme = table.Column<int>(nullable: true),
                    id_contact_organisme = table.Column<int>(nullable: true),
                    id_potentiel = table.Column<int>(nullable: true),
                    id_action = table.Column<int>(nullable: true),
                    id_url_action = table.Column<int>(nullable: true),
                    id_periode = table.Column<int>(nullable: true),
                    id_adresse_inscription = table.Column<int>(nullable: true),
                    id_date_information = table.Column<int>(nullable: true),
                    id_organisme_formateur = table.Column<int>(nullable: true),
                    id_contact_formateur = table.Column<int>(nullable: true),
                    id_organisme_financeur = table.Column<int>(nullable: true),
                    commentaire = table.Column<string>(unicode: false, maxLength: 300, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Extra", x => x.id_extra);
                    table.ForeignKey(
                        name: "fk_id_action_extra",
                        column: x => x.id_action,
                        principalTable: "_Action",
                        principalColumn: "id_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_extra",
                        column: x => x.id_adresse,
                        principalTable: "Adresse",
                        principalColumn: "id_adresse",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_adresse_inscription_extra",
                        column: x => x.id_adresse_inscription,
                        principalTable: "AdresseInscription",
                        principalColumn: "id_adresse_inscription",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_certification_extra",
                        column: x => x.id_certification,
                        principalTable: "Certification",
                        principalColumn: "id_certification",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formateur_extra",
                        column: x => x.id_contact_formateur,
                        principalTable: "ContactFormateur",
                        principalColumn: "id_contact_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_formation_extra",
                        column: x => x.id_contact_formation,
                        principalTable: "ContactFormation",
                        principalColumn: "id_contact_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_contact_organisme_extra",
                        column: x => x.id_contact_organisme,
                        principalTable: "ContactOrganisme",
                        principalColumn: "id_contact_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_extra",
                        column: x => x.id_coordonnee,
                        principalTable: "Coordonnee",
                        principalColumn: "id_coordonnee",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_coordonnee_organisme_extra",
                        column: x => x.id_coordonnee_organisme,
                        principalTable: "CoordonneeOrganisme",
                        principalColumn: "id_coordonnee_organisme",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_date_information_extra",
                        column: x => x.id_date_information,
                        principalTable: "DateInformation",
                        principalColumn: "id_date_information",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_domaine_formation_extra",
                        column: x => x.id_domaine_formation,
                        principalTable: "DomaineFormation",
                        principalColumn: "id_domaine_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_formation_extra",
                        column: x => x.id_formation,
                        principalTable: "Formation",
                        principalColumn: "id_formation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_geolocalisation_extra",
                        column: x => x.id_geolocalisation,
                        principalTable: "Geolocalisation",
                        principalColumn: "id_geolocalisation",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_lheo_extra",
                        column: x => x.id_lheo,
                        principalTable: "Lheo",
                        principalColumn: "id_lheo",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_module_extra",
                        column: x => x.id_module,
                        principalTable: "Module",
                        principalColumn: "id_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_modules_prerequis_extra",
                        column: x => x.id_modules_prerequis,
                        principalTable: "ModulePrerequis",
                        principalColumn: "id_modules_prerequis",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_offre_extra",
                        column: x => x.id_offre,
                        principalTable: "Offre",
                        principalColumn: "id_offre",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_financeur_extra",
                        column: x => x.id_organisme_financeur,
                        principalTable: "OrganismeFinanceur",
                        principalColumn: "id_organisme_financeur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formateur_extra",
                        column: x => x.id_organisme_formateur,
                        principalTable: "OrganismeFormateur",
                        principalColumn: "id_organisme_formateur",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_organisme_formation_responsable_extra",
                        column: x => x.id_organisme_formation_responsable,
                        principalTable: "OrganismeFormationResponsable",
                        principalColumn: "id_organisme_formation_responsable",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_periode_extra",
                        column: x => x.id_periode,
                        principalTable: "Periode",
                        principalColumn: "id_periode",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_potentiel_extra",
                        column: x => x.id_potentiel,
                        principalTable: "Potentiel",
                        principalColumn: "id_potentiel",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_SIRET_extra",
                        column: x => x.id_SIRET,
                        principalTable: "SIRET",
                        principalColumn: "id_SIRET",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_sous_module_extra",
                        column: x => x.id_sous_module,
                        principalTable: "SousModule",
                        principalColumn: "id_sous_module",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_url_action_extra",
                        column: x => x.id_url_action,
                        principalTable: "UrlAction",
                        principalColumn: "id_url_action",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "fk_id_web_extra",
                        column: x => x.id_web,
                        principalTable: "Web",
                        principalColumn: "id_web",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX__Action_code_perimetre_recrutement",
                table: "_Action",
                column: "code_perimetre_recrutement");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_adresse_information",
                table: "_Action",
                column: "id_adresse_information");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_lieu_de_formation",
                table: "_Action",
                column: "id_lieu_de_formation");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_modalite_enseignement",
                table: "_Action",
                column: "id_modalite_enseignement");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_organisme_formateur",
                table: "_Action",
                column: "id_organisme_formateur");

            migrationBuilder.CreateIndex(
                name: "IX__Action_id_url_action",
                table: "_Action",
                column: "id_url_action");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_action",
                table: "_Session",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_adresse_inscription",
                table: "_Session",
                column: "id_adresse_inscription");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_etat_recrutement",
                table: "_Session",
                column: "id_etat_recrutement");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_periode",
                table: "_Session",
                column: "id_periode");

            migrationBuilder.CreateIndex(
                name: "IX__Session_id_periode_inscription",
                table: "_Session",
                column: "id_periode_inscription");

            migrationBuilder.CreateIndex(
                name: "IX_ActionOrganismeFinanceur_id_action",
                table: "ActionOrganismeFinanceur",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_ActionOrganismeFinanceur_id_organisme_financeur",
                table: "ActionOrganismeFinanceur",
                column: "id_organisme_financeur");

            migrationBuilder.CreateIndex(
                name: "IX_Adresse_id_geolocalisation",
                table: "Adresse",
                column: "id_geolocalisation");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseInformation_id_adresse",
                table: "AdresseInformation",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_AdresseInscription_id_adresse",
                table: "AdresseInscription",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_CodePublicVise_id_action",
                table: "CodePublicVise",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_ContactFormateur_id_coordonnee",
                table: "ContactFormateur",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactFormation_id_coordonnee",
                table: "ContactFormation",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactOrganisme_id_coordonnee",
                table: "ContactOrganisme",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ContactTel_id_numtel",
                table: "ContactTel",
                column: "id_numtel");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_adresse",
                table: "Coordonnee",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_fax",
                table: "Coordonnee",
                column: "id_fax");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_portable",
                table: "Coordonnee",
                column: "id_portable");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_telfixe",
                table: "Coordonnee",
                column: "id_telfixe");

            migrationBuilder.CreateIndex(
                name: "IX_Coordonnee_id_web",
                table: "Coordonnee",
                column: "id_web");

            migrationBuilder.CreateIndex(
                name: "IX_CoordonneeOrganisme_id_coordonnee",
                table: "CoordonneeOrganisme",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_DateInformation_id_action",
                table: "DateInformation",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_action",
                table: "Extra",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_adresse",
                table: "Extra",
                column: "id_adresse");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_adresse_inscription",
                table: "Extra",
                column: "id_adresse_inscription");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_certification",
                table: "Extra",
                column: "id_certification");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_formateur",
                table: "Extra",
                column: "id_contact_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_formation",
                table: "Extra",
                column: "id_contact_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_contact_organisme",
                table: "Extra",
                column: "id_contact_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_coordonnee",
                table: "Extra",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_coordonnee_organisme",
                table: "Extra",
                column: "id_coordonnee_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_date_information",
                table: "Extra",
                column: "id_date_information");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_domaine_formation",
                table: "Extra",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_formation",
                table: "Extra",
                column: "id_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_geolocalisation",
                table: "Extra",
                column: "id_geolocalisation");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_lheo",
                table: "Extra",
                column: "id_lheo");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_module",
                table: "Extra",
                column: "id_module");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_modules_prerequis",
                table: "Extra",
                column: "id_modules_prerequis");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_offre",
                table: "Extra",
                column: "id_offre");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_financeur",
                table: "Extra",
                column: "id_organisme_financeur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_formateur",
                table: "Extra",
                column: "id_organisme_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_organisme_formation_responsable",
                table: "Extra",
                column: "id_organisme_formation_responsable");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_periode",
                table: "Extra",
                column: "id_periode");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_potentiel",
                table: "Extra",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_SIRET",
                table: "Extra",
                column: "id_SIRET");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_sous_module",
                table: "Extra",
                column: "id_sous_module");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_url_action",
                table: "Extra",
                column: "id_url_action");

            migrationBuilder.CreateIndex(
                name: "IX_Extra_id_web",
                table: "Extra",
                column: "id_web");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_code_niveau_sortie",
                table: "Formation",
                column: "id_code_niveau_sortie");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_contact_formation",
                table: "Formation",
                column: "id_contact_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_domaine_formation",
                table: "Formation",
                column: "id_domaine_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_modules_prerequis",
                table: "Formation",
                column: "id_modules_prerequis");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_niveaux_entree",
                table: "Formation",
                column: "id_niveaux_entree");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_objectif_general_formation",
                table: "Formation",
                column: "id_objectif_general_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_organisme_formation_responsable",
                table: "Formation",
                column: "id_organisme_formation_responsable");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_parcours_de_formation",
                table: "Formation",
                column: "id_parcours_de_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_positionnement",
                table: "Formation",
                column: "id_positionnement");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_sous_module",
                table: "Formation",
                column: "id_sous_module");

            migrationBuilder.CreateIndex(
                name: "IX_Formation_id_url_formation",
                table: "Formation",
                column: "id_url_formation");

            migrationBuilder.CreateIndex(
                name: "IX_Lheo_id_offre",
                table: "Lheo",
                column: "id_offre");

            migrationBuilder.CreateIndex(
                name: "IX_LieuDeFormation_id_coordonnee",
                table: "LieuDeFormation",
                column: "id_coordonnee");

            migrationBuilder.CreateIndex(
                name: "IX_ModalitePedagogique_id_action",
                table: "ModalitePedagogique",
                column: "id_action");

            migrationBuilder.CreateIndex(
                name: "IX_Module_id_type_module",
                table: "Module",
                column: "id_type_module");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_contact_formateur",
                table: "OrganismeFormateur",
                column: "id_contact_formateur");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_potentiel",
                table: "OrganismeFormateur",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormateur_id_siret_formation",
                table: "OrganismeFormateur",
                column: "id_siret_formation");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_contact_organisme",
                table: "OrganismeFormationResponsable",
                column: "id_contact_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_coordonnee_organisme",
                table: "OrganismeFormationResponsable",
                column: "id_coordonnee_organisme");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_potentiel",
                table: "OrganismeFormationResponsable",
                column: "id_potentiel");

            migrationBuilder.CreateIndex(
                name: "IX_OrganismeFormationResponsable_id_SIRET",
                table: "OrganismeFormationResponsable",
                column: "id_SIRET");

            migrationBuilder.CreateIndex(
                name: "UK_perimetre_recrutement_cle",
                table: "PerimetreRecrutement",
                column: "cle",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserRole_IdUser",
                table: "UserRole",
                column: "IdUser");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "_Session");

            migrationBuilder.DropTable(
                name: "ActionOrganismeFinanceur");

            migrationBuilder.DropTable(
                name: "CodeFORMACODE");

            migrationBuilder.DropTable(
                name: "CodeNSF");

            migrationBuilder.DropTable(
                name: "CodePublicVise");

            migrationBuilder.DropTable(
                name: "CodeROME");

            migrationBuilder.DropTable(
                name: "Extra");

            migrationBuilder.DropTable(
                name: "Ligne");

            migrationBuilder.DropTable(
                name: "ModalitePedagogique");

            migrationBuilder.DropTable(
                name: "ReferenceModule");

            migrationBuilder.DropTable(
                name: "UrlWeb");

            migrationBuilder.DropTable(
                name: "UserRole");

            migrationBuilder.DropTable(
                name: "EtatRecrutement");

            migrationBuilder.DropTable(
                name: "AdresseInscription");

            migrationBuilder.DropTable(
                name: "Certification");

            migrationBuilder.DropTable(
                name: "DateInformation");

            migrationBuilder.DropTable(
                name: "Formation");

            migrationBuilder.DropTable(
                name: "Lheo");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "OrganismeFinanceur");

            migrationBuilder.DropTable(
                name: "Periode");

            migrationBuilder.DropTable(
                name: "UrlAction");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "_Action");

            migrationBuilder.DropTable(
                name: "Niveaux");

            migrationBuilder.DropTable(
                name: "ContactFormation");

            migrationBuilder.DropTable(
                name: "DomaineFormation");

            migrationBuilder.DropTable(
                name: "ModulePrerequis");

            migrationBuilder.DropTable(
                name: "ObjectifGeneralFormation");

            migrationBuilder.DropTable(
                name: "OrganismeFormationResponsable");

            migrationBuilder.DropTable(
                name: "ParcoursDeFormation");

            migrationBuilder.DropTable(
                name: "Positionnement");

            migrationBuilder.DropTable(
                name: "SousModule");

            migrationBuilder.DropTable(
                name: "UrlFormation");

            migrationBuilder.DropTable(
                name: "Offre");

            migrationBuilder.DropTable(
                name: "TypeModule");

            migrationBuilder.DropTable(
                name: "PerimetreRecrutement");

            migrationBuilder.DropTable(
                name: "AdresseInformation");

            migrationBuilder.DropTable(
                name: "LieuDeFormation");

            migrationBuilder.DropTable(
                name: "ModaliteEnseignement");

            migrationBuilder.DropTable(
                name: "OrganismeFormateur");

            migrationBuilder.DropTable(
                name: "ContactOrganisme");

            migrationBuilder.DropTable(
                name: "CoordonneeOrganisme");

            migrationBuilder.DropTable(
                name: "ContactFormateur");

            migrationBuilder.DropTable(
                name: "Potentiel");

            migrationBuilder.DropTable(
                name: "SIRET");

            migrationBuilder.DropTable(
                name: "Coordonnee");

            migrationBuilder.DropTable(
                name: "Adresse");

            migrationBuilder.DropTable(
                name: "ContactTel");

            migrationBuilder.DropTable(
                name: "Web");

            migrationBuilder.DropTable(
                name: "Geolocalisation");

            migrationBuilder.DropTable(
                name: "Numtel");
        }
    }
}
