﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class OrganismeFinanceur
    {
        public OrganismeFinanceur()
        {
            ActionOrganismeFinanceur = new HashSet<ActionOrganismeFinanceur>();
            Extra = new HashSet<Extra>();
        }

        public int IdOrganismeFinanceur { get; set; }
        public int CodeFinanceur { get; set; }
        public int? NbPlaceFinancees { get; set; }

        public ICollection<ActionOrganismeFinanceur> ActionOrganismeFinanceur { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
