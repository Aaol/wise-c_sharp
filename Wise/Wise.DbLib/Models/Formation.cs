﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Formation
    {
        public Formation()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdFormation { get; set; }
        public int IdDomaineFormation { get; set; }
        public string IntituleFormation { get; set; }
        public string ObjectifFormation { get; set; }
        public string ResultatsAttendus { get; set; }
        public string ContenuFormation { get; set; }
        public bool Certifiante { get; set; }
        public int IdContactFormation { get; set; }
        public int IdParcoursDeFormation { get; set; }
        public int IdNiveauxEntree { get; set; }
        public int? IdObjectifGeneralFormation { get; set; }
        public int? IdCodeNiveauSortie { get; set; }
        public int? IdUrlFormation { get; set; }
        public int IdOrganismeFormationResponsable { get; set; }
        public string IdentifiantModule { get; set; }
        public int? IdPositionnement { get; set; }
        public int? IdSousModule { get; set; }
        public int? IdModulesPrerequis { get; set; }

        public Niveaux IdCodeNiveauSortieNavigation { get; set; }
        public ContactFormation IdContactFormationNavigation { get; set; }
        public DomaineFormation IdDomaineFormationNavigation { get; set; }
        public ModulePrerequis IdModulesPrerequisNavigation { get; set; }
        public Niveaux IdNiveauxEntreeNavigation { get; set; }
        public ObjectifGeneralFormation IdObjectifGeneralFormationNavigation { get; set; }
        public OrganismeFormationResponsable IdOrganismeFormationResponsableNavigation { get; set; }
        public ParcoursDeFormation IdParcoursDeFormationNavigation { get; set; }
        public Positionnement IdPositionnementNavigation { get; set; }
        public SousModule IdSousModuleNavigation { get; set; }
        public UrlFormation IdUrlFormationNavigation { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
