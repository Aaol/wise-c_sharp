﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlWeb
    {
        public int IdUrlWeb { get; set; }
        public string Url { get; set; }
    }
}
