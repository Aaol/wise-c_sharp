﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ContactOrganisme
    {
        public ContactOrganisme()
        {
            Extra = new HashSet<Extra>();
            OrganismeFormationResponsable = new HashSet<OrganismeFormationResponsable>();
        }

        public int IdContactOrganisme { get; set; }
        public int IdCoordonnee { get; set; }

        public Coordonnee IdCoordonneeNavigation { get; set; }
        public ICollection<Extra> Extra { get; set; }
        public ICollection<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
    }
}
