﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wise.DbLib.Models
{
    /// <summary>
    ///     Table de jointure pour affecter une liste de rôle aux utilisateurs
    /// </summary>
    public class UserRole
    {
        #region Properties

        public long IdUser { get; set; }
        public int IdRole { get; set; }
        public User User { get; set; }
        public Role Role { get; set; }

        #endregion
    }
}
