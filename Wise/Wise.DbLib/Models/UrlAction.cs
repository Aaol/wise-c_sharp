﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlAction
    {
        public UrlAction()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdUrlAction { get; set; }

        public ICollection<Extra> Extra { get; set; }
    }
}
