﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class UrlFormation
    {
        public UrlFormation()
        {
            Formation = new HashSet<Formation>();
        }

        public int IdUrlFormation { get; set; }

        public ICollection<Formation> Formation { get; set; }
    }
}
