﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Geolocalisation
    {
        public Geolocalisation()
        {
            Adresse = new HashSet<Adresse>();
            Extra = new HashSet<Extra>();
        }

        public int IdGeolocalisation { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public ICollection<Adresse> Adresse { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
