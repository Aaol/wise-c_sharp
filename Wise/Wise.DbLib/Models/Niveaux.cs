﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Niveaux
    {
        public Niveaux()
        {
            FormationIdCodeNiveauSortieNavigation = new HashSet<Formation>();
            FormationIdNiveauxEntreeNavigation = new HashSet<Formation>();
        }

        public int IdNiveaux { get; set; }
        public string Libele { get; set; }

        public ICollection<Formation> FormationIdCodeNiveauSortieNavigation { get; set; }
        public ICollection<Formation> FormationIdNiveauxEntreeNavigation { get; set; }
    }
}
