﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Module
    {
        public Module()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdModule { get; set; }
        public string ReferenceModule { get; set; }
        public int IdTypeModule { get; set; }

        public TypeModule IdTypeModuleNavigation { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
