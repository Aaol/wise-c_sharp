﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ReferenceModule
    {
        public int IdReferenceModule { get; set; }
        public string Reference { get; set; }
    }
}
