﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ContactTel
    {
        public ContactTel()
        {
            CoordonneeIdFaxNavigation = new HashSet<Coordonnee>();
            CoordonneeIdPortableNavigation = new HashSet<Coordonnee>();
            CoordonneeIdTelfixeNavigation = new HashSet<Coordonnee>();
        }

        public int IdContactTel { get; set; }
        public int IdNumtel { get; set; }

        public Numtel IdNumtelNavigation { get; set; }
        public ICollection<Coordonnee> CoordonneeIdFaxNavigation { get; set; }
        public ICollection<Coordonnee> CoordonneeIdPortableNavigation { get; set; }
        public ICollection<Coordonnee> CoordonneeIdTelfixeNavigation { get; set; }
    }
}
