﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeRome
    {
        public int IdCodeRome { get; set; }
        public string Code { get; set; }
    }
}
