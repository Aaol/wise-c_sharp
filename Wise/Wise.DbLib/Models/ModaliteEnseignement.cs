﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ModaliteEnseignement
    {
        public ModaliteEnseignement()
        {
            Action = new HashSet<Action>();
        }

        public int IdModaliteEnseignement { get; set; }
        public string Label { get; set; }
        public int ModaliteKey { get; set; }

        public ICollection<Action> Action { get; set; }
    }
}
