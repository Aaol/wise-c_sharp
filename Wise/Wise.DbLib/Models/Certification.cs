﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Certification
    {
        public Certification()
        {
            Extra = new HashSet<Extra>();
        }

        public int IdCertification { get; set; }
        public string CodeRncp { get; set; }
        public string CodeCertifinfo { get; set; }

        public ICollection<Extra> Extra { get; set; }
    }
}
