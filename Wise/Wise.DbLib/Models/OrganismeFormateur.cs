﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class OrganismeFormateur
    {
        public OrganismeFormateur()
        {
            Action = new HashSet<Action>();
            Extra = new HashSet<Extra>();
        }

        public int IdOrganismeFormateur { get; set; }
        public int IdSiretFormation { get; set; }
        public string RaisonSocialeFormateur { get; set; }
        public int? IdContactFormateur { get; set; }
        public int? IdPotentiel { get; set; }

        public ContactFormateur IdContactFormateurNavigation { get; set; }
        public Potentiel IdPotentielNavigation { get; set; }
        public Siret IdSiretFormationNavigation { get; set; }
        public ICollection<Action> Action { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
