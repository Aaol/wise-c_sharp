﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Offre
    {
        public Offre()
        {
            Extra = new HashSet<Extra>();
            Lheo = new HashSet<Lheo>();
        }

        public int IdOffre { get; set; }

        public ICollection<Extra> Extra { get; set; }
        public ICollection<Lheo> Lheo { get; set; }
    }
}
