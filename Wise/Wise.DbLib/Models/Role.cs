﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wise.DbLib.Models
{
    /// <summary>
    ///     Classe représentant un rôle utilisateur
    /// </summary>
    public class Role
    {
        #region Properties
        
        /// <summary>
        ///     Identifiant du rôle utilisateur.
        /// </summary>
        [Key]
        public int IdRole { get; set; }

        /// <summary>
        ///     Nom du rôle de l'utilisateur.
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        ///     Utilisateurs associés au rôle.
        /// </summary>
        public List<UserRole> UserRoles { get; set; }

        #endregion
    }
}
