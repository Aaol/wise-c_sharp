﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Adresse
    {
        public Adresse()
        {
            AdresseInformation = new HashSet<AdresseInformation>();
            AdresseInscription = new HashSet<AdresseInscription>();
            Coordonnee = new HashSet<Coordonnee>();
            Extra = new HashSet<Extra>();
            Session = new HashSet<Session>();
        }

        public int IdAdresse { get; set; }
        public string Codepostal { get; set; }
        public string Ville { get; set; }
        public string Departement { get; set; }
        public string CodeInseeCommune { get; set; }
        public string CodeInseeCanton { get; set; }
        public string Region { get; set; }
        public string Pays { get; set; }
        public int? IdGeolocalisation { get; set; }

        public Geolocalisation IdGeolocalisationNavigation { get; set; }
        public ICollection<AdresseInformation> AdresseInformation { get; set; }
        public ICollection<AdresseInscription> AdresseInscription { get; set; }
        public ICollection<Coordonnee> Coordonnee { get; set; }
        public ICollection<Extra> Extra { get; set; }
        public ICollection<Session> Session { get; set; }
    }
}
