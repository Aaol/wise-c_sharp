﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class EtatRecrutement
    {
        public EtatRecrutement()
        {
            Session = new HashSet<Session>();
        }

        public int IdEtatRecrutement { get; set; }
        public string Label { get; set; }
        public int RecrutementKey { get; set; }

        public ICollection<Session> Session { get; set; }
    }
}
