﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ContactFormateur
    {
        public ContactFormateur()
        {
            Extra = new HashSet<Extra>();
            OrganismeFormateur = new HashSet<OrganismeFormateur>();
        }

        public int IdContactFormateur { get; set; }
        public int IdCoordonnee { get; set; }

        public Coordonnee IdCoordonneeNavigation { get; set; }
        public ICollection<Extra> Extra { get; set; }
        public ICollection<OrganismeFormateur> OrganismeFormateur { get; set; }
    }
}
