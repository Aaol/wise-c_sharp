﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Positionnement
    {
        public Positionnement()
        {
            Formation = new HashSet<Formation>();
        }

        public int IdPositionnement { get; set; }
        public string Libele { get; set; }

        public ICollection<Formation> Formation { get; set; }
    }
}
