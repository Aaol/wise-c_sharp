﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class TypeModule
    {
        public TypeModule()
        {
            Module = new HashSet<Module>();
        }

        public int IdTypeModule { get; set; }
        public string Libele { get; set; }

        public ICollection<Module> Module { get; set; }
    }
}
