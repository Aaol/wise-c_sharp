﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Coordonnee
    {
        public Coordonnee()
        {
            ContactFormateur = new HashSet<ContactFormateur>();
            ContactFormation = new HashSet<ContactFormation>();
            ContactOrganisme = new HashSet<ContactOrganisme>();
            CoordonneeOrganisme = new HashSet<CoordonneeOrganisme>();
            Extra = new HashSet<Extra>();
            LieuDeFormation = new HashSet<LieuDeFormation>();
        }

        public int IdCoordonnee { get; set; }
        public string Civilite { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public int? IdAdresse { get; set; }
        public int IdTelfixe { get; set; }
        public int IdPortable { get; set; }
        public int IdFax { get; set; }
        public string Courriel { get; set; }
        public int? IdWeb { get; set; }

        public Adresse IdAdresseNavigation { get; set; }
        public ContactTel IdFaxNavigation { get; set; }
        public ContactTel IdPortableNavigation { get; set; }
        public ContactTel IdTelfixeNavigation { get; set; }
        public Web IdWebNavigation { get; set; }
        public ICollection<ContactFormateur> ContactFormateur { get; set; }
        public ICollection<ContactFormation> ContactFormation { get; set; }
        public ICollection<ContactOrganisme> ContactOrganisme { get; set; }
        public ICollection<CoordonneeOrganisme> CoordonneeOrganisme { get; set; }
        public ICollection<Extra> Extra { get; set; }
        public ICollection<LieuDeFormation> LieuDeFormation { get; set; }
    }
}
