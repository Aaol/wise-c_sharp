﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class SousModule
    {
        public SousModule()
        {
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
        }

        public int IdSousModule { get; set; }

        public ICollection<Extra> Extra { get; set; }
        public ICollection<Formation> Formation { get; set; }
    }
}
