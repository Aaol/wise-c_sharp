﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Potentiel
    {
        public Potentiel()
        {
            Extra = new HashSet<Extra>();
            OrganismeFormateur = new HashSet<OrganismeFormateur>();
            OrganismeFormationResponsable = new HashSet<OrganismeFormationResponsable>();
        }

        public int IdPotentiel { get; set; }

        public ICollection<Extra> Extra { get; set; }
        public ICollection<OrganismeFormateur> OrganismeFormateur { get; set; }
        public ICollection<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
    }
}
