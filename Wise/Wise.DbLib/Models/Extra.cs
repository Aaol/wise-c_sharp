﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Extra
    {
        public int IdExtra { get; set; }
        public int? IdLheo { get; set; }
        public int? IdOffre { get; set; }
        public int? IdFormation { get; set; }
        public int? IdDomaineFormation { get; set; }
        public int? IdContactFormation { get; set; }
        public int? IdCoordonnee { get; set; }
        public int? IdAdresse { get; set; }
        public int? IdGeolocalisation { get; set; }
        public int? IdWeb { get; set; }
        public int? IdCertification { get; set; }
        public int? IdSousModule { get; set; }
        public int? IdModule { get; set; }
        public int? IdModulesPrerequis { get; set; }
        public int? IdOrganismeFormationResponsable { get; set; }
        public int? IdSiret { get; set; }
        public int? IdCoordonneeOrganisme { get; set; }
        public int? IdContactOrganisme { get; set; }
        public int? IdPotentiel { get; set; }
        public int? IdAction { get; set; }
        public int? IdUrlAction { get; set; }
        public int? IdPeriode { get; set; }
        public int? IdAdresseInscription { get; set; }
        public int? IdDateInformation { get; set; }
        public int? IdOrganismeFormateur { get; set; }
        public int? IdContactFormateur { get; set; }
        public int? IdOrganismeFinanceur { get; set; }
        public string Commentaire { get; set; }

        public Action IdActionNavigation { get; set; }
        public AdresseInscription IdAdresseInscriptionNavigation { get; set; }
        public Adresse IdAdresseNavigation { get; set; }
        public Certification IdCertificationNavigation { get; set; }
        public ContactFormateur IdContactFormateurNavigation { get; set; }
        public ContactFormation IdContactFormationNavigation { get; set; }
        public ContactOrganisme IdContactOrganismeNavigation { get; set; }
        public Coordonnee IdCoordonneeNavigation { get; set; }
        public CoordonneeOrganisme IdCoordonneeOrganismeNavigation { get; set; }
        public DateInformation IdDateInformationNavigation { get; set; }
        public DomaineFormation IdDomaineFormationNavigation { get; set; }
        public Formation IdFormationNavigation { get; set; }
        public Geolocalisation IdGeolocalisationNavigation { get; set; }
        public Lheo IdLheoNavigation { get; set; }
        public Module IdModuleNavigation { get; set; }
        public ModulePrerequis IdModulesPrerequisNavigation { get; set; }
        public Offre IdOffreNavigation { get; set; }
        public OrganismeFinanceur IdOrganismeFinanceurNavigation { get; set; }
        public OrganismeFormateur IdOrganismeFormateurNavigation { get; set; }
        public OrganismeFormationResponsable IdOrganismeFormationResponsableNavigation { get; set; }
        public Periode IdPeriodeNavigation { get; set; }
        public Potentiel IdPotentielNavigation { get; set; }
        public Siret IdSiretNavigation { get; set; }
        public SousModule IdSousModuleNavigation { get; set; }
        public UrlAction IdUrlActionNavigation { get; set; }
        public Web IdWebNavigation { get; set; }
    }
}
