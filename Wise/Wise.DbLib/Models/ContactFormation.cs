﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class ContactFormation
    {
        public ContactFormation()
        {
            Extra = new HashSet<Extra>();
            Formation = new HashSet<Formation>();
        }

        public int IdContactFormation { get; set; }
        public int IdCoordonnee { get; set; }

        public Coordonnee IdCoordonneeNavigation { get; set; }
        public ICollection<Extra> Extra { get; set; }
        public ICollection<Formation> Formation { get; set; }
    }
}
