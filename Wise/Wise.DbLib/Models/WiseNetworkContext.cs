﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Wise.DbLib.Models
{
    public partial class WiseNetworkContext : DbContext
    {
        public WiseNetworkContext()
        {
        }

        public WiseNetworkContext(DbContextOptions<WiseNetworkContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Action> Action { get; set; }
        public virtual DbSet<ActionOrganismeFinanceur> ActionOrganismeFinanceur { get; set; }
        public virtual DbSet<Adresse> Adresse { get; set; }
        public virtual DbSet<AdresseInformation> AdresseInformation { get; set; }
        public virtual DbSet<AdresseInscription> AdresseInscription { get; set; }
        public virtual DbSet<Certification> Certification { get; set; }
        public virtual DbSet<CodeFormacode> CodeFormacode { get; set; }
        public virtual DbSet<CodeNsf> CodeNsf { get; set; }
        public virtual DbSet<CodePublicVise> CodePublicVise { get; set; }
        public virtual DbSet<CodeRome> CodeRome { get; set; }
        public virtual DbSet<ContactFormateur> ContactFormateur { get; set; }
        public virtual DbSet<ContactFormation> ContactFormation { get; set; }
        public virtual DbSet<ContactOrganisme> ContactOrganisme { get; set; }
        public virtual DbSet<ContactTel> ContactTel { get; set; }
        public virtual DbSet<Coordonnee> Coordonnee { get; set; }
        public virtual DbSet<CoordonneeOrganisme> CoordonneeOrganisme { get; set; }
        public virtual DbSet<DateInformation> DateInformation { get; set; }
        public virtual DbSet<DomaineFormation> DomaineFormation { get; set; }
        public virtual DbSet<EtatRecrutement> EtatRecrutement { get; set; }
        public virtual DbSet<Extra> Extra { get; set; }
        public virtual DbSet<Formation> Formation { get; set; }
        public virtual DbSet<Geolocalisation> Geolocalisation { get; set; }
        public virtual DbSet<Lheo> Lheo { get; set; }
        public virtual DbSet<LieuDeFormation> LieuDeFormation { get; set; }
        public virtual DbSet<Ligne> Ligne { get; set; }
        public virtual DbSet<ModaliteEnseignement> ModaliteEnseignement { get; set; }
        public virtual DbSet<ModalitePedagogique> ModalitePedagogique { get; set; }
        public virtual DbSet<Module> Module { get; set; }
        public virtual DbSet<ModulePrerequis> ModulePrerequis { get; set; }
        public virtual DbSet<Niveaux> Niveaux { get; set; }
        public virtual DbSet<Numtel> Numtel { get; set; }
        public virtual DbSet<ObjectifGeneralFormation> ObjectifGeneralFormation { get; set; }
        public virtual DbSet<Offre> Offre { get; set; }
        public virtual DbSet<OrganismeFinanceur> OrganismeFinanceur { get; set; }
        public virtual DbSet<OrganismeFormateur> OrganismeFormateur { get; set; }
        public virtual DbSet<OrganismeFormationResponsable> OrganismeFormationResponsable { get; set; }
        public virtual DbSet<ParcoursDeFormation> ParcoursDeFormation { get; set; }
        public virtual DbSet<PerimetreRecrutement> PerimetreRecrutement { get; set; }
        public virtual DbSet<Periode> Periode { get; set; }
        public virtual DbSet<Positionnement> Positionnement { get; set; }
        public virtual DbSet<Potentiel> Potentiel { get; set; }
        public virtual DbSet<ReferenceModule> ReferenceModule { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<Siret> Siret { get; set; }
        public virtual DbSet<SousModule> SousModule { get; set; }
        public virtual DbSet<TypeModule> TypeModule { get; set; }
        public virtual DbSet<UrlAction> UrlAction { get; set; }
        public virtual DbSet<UrlFormation> UrlFormation { get; set; }
        public virtual DbSet<UrlWeb> UrlWeb { get; set; }
        public virtual DbSet<Web> Web { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }

        // Unable to generate entity type for table 'dbo.OffreFormation'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DomaineFormationCodeFORMACODE'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DomaineFormationCodeNSF'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.DomaineFormationCodeROME'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.CoordonneeLigne'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.AdresseLigne'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.WebUrlWeb'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UrlFormationUrlWeb'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FormationCertification'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.FormationAction'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.SousModuleModule'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.ModulePrerequisReferenceModule'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.PotentielCodeFORMACODE'. Please see the warning messages.
        // Unable to generate entity type for table 'dbo.UrlActionUrlWeb'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=localhost;Database=WiseNetwork;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Action>(entity =>
            {
                entity.HasKey(e => e.IdAction);

                entity.ToTable("_Action");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.AccesHandicape)
                    .HasColumnName("acces_handicape")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CodePerimetreRecrutement).HasColumnName("code_perimetre_recrutement");

                entity.Property(e => e.ConditionSpecifiques)
                    .IsRequired()
                    .HasColumnName("condition_specifiques")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Conventionnement).HasColumnName("conventionnement");

                entity.Property(e => e.DetailConditionPriseEnCharge)
                    .HasColumnName("detail_condition_prise_en_charge")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.DureeConventionee).HasColumnName("duree_conventionee");

                entity.Property(e => e.DureeIndicative)
                    .HasColumnName("duree_indicative")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FraisRestant)
                    .HasColumnName("frais_restant")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Hebergement)
                    .HasColumnName("hebergement")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdresseInformation).HasColumnName("id_adresse_information");

                entity.Property(e => e.IdLieuDeFormation).HasColumnName("id_lieu_de_formation");

                entity.Property(e => e.IdModaliteEnseignement).HasColumnName("id_modalite_enseignement");

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");

                entity.Property(e => e.InfoPerimetreRecrutement)
                    .HasColumnName("info_perimetre_recrutement")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InfoPublicVise)
                    .HasColumnName("info_public_vise")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.LangeFormation)
                    .HasColumnName("lange_formation")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteAlternance)
                    .HasColumnName("modalite_alternance")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ModalitePedagogiques)
                    .HasColumnName("modalite_pedagogiques")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteRecrutement)
                    .HasColumnName("modalite_recrutement")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ModalitesEntreesSorties).HasColumnName("modalites_entrees_sorties");

                entity.Property(e => e.NiveauEntreeObligatoire).HasColumnName("niveau_entree_obligatoire");

                entity.Property(e => e.NombreHeuresCentre).HasColumnName("nombre_heures_centre");

                entity.Property(e => e.NombreHeuresEnterprise).HasColumnName("nombre_heures_enterprise");

                entity.Property(e => e.NombreHeuresTotal).HasColumnName("nombre_heures_total");

                entity.Property(e => e.PriseEnChargeFraisPossible).HasColumnName("prise_en_charge_frais_possible");

                entity.Property(e => e.PrixHoraireTtc).HasColumnName("prix_horaire_ttc");

                entity.Property(e => e.PrixTotalTtc).HasColumnName("prix_total_ttc");

                entity.Property(e => e.Restauration)
                    .HasColumnName("restauration")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RythmeFormation)
                    .IsRequired()
                    .HasColumnName("rythme_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.Transport)
                    .HasColumnName("transport")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.CodePerimetreRecrutementNavigation)
                    .WithMany(p => p.Action)
                    .HasPrincipalKey(p => p.Cle)
                    .HasForeignKey(d => d.CodePerimetreRecrutement)
                    .HasConstraintName("fk_code_perimetre_recrutement_action");

                entity.HasOne(d => d.IdAdresseInformationNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdAdresseInformation)
                    .HasConstraintName("fk_action_id_adresse_information");

                entity.HasOne(d => d.IdLieuDeFormationNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdLieuDeFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_lieu_de_formation_action");

                entity.HasOne(d => d.IdModaliteEnseignementNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdModaliteEnseignement)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_modalite_enseignement_action");

                entity.HasOne(d => d.IdOrganismeFormateurNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdOrganismeFormateur)
                    .HasConstraintName("fk_id_organisme_formateur_action");

                entity.HasOne(d => d.IdUrlActionNavigation)
                    .WithMany(p => p.Action)
                    .HasForeignKey(d => d.IdUrlAction)
                    .HasConstraintName("fk_id_url_action_action");
            });

            modelBuilder.Entity<ActionOrganismeFinanceur>(entity =>
            {
                entity.HasKey(e => e.IdActionOrganismeFinanceur);

                entity.Property(e => e.IdActionOrganismeFinanceur).HasColumnName("id_action_organisme_financeur");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.ActionOrganismeFinanceur)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_action_id_action");

                entity.HasOne(d => d.IdOrganismeFinanceurNavigation)
                    .WithMany(p => p.ActionOrganismeFinanceur)
                    .HasForeignKey(d => d.IdOrganismeFinanceur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_action_id_organisme_financeur");
            });

            modelBuilder.Entity<Adresse>(entity =>
            {
                entity.HasKey(e => e.IdAdresse);

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.CodeInseeCanton)
                    .HasColumnName("code_INSEE_canton")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CodeInseeCommune)
                    .HasColumnName("code_INSEE_commune")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Codepostal)
                    .IsRequired()
                    .HasColumnName("codepostal")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Departement)
                    .HasColumnName("departement")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.Pays)
                    .HasColumnName("pays")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasColumnName("region")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Ville)
                    .IsRequired()
                    .HasColumnName("ville")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdGeolocalisationNavigation)
                    .WithMany(p => p.Adresse)
                    .HasForeignKey(d => d.IdGeolocalisation)
                    .HasConstraintName("fk_id_geolocalisation_adresse");
            });

            modelBuilder.Entity<AdresseInformation>(entity =>
            {
                entity.HasKey(e => e.IdAdresseInformation);

                entity.Property(e => e.IdAdresseInformation).HasColumnName("id_adresse_information");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.AdresseInformation)
                    .HasForeignKey(d => d.IdAdresse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_adresse_information_id_adresse");
            });

            modelBuilder.Entity<AdresseInscription>(entity =>
            {
                entity.HasKey(e => e.IdAdresseInscription);

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.AdresseInscription)
                    .HasForeignKey(d => d.IdAdresse)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_adresse_adresse_inscription");
            });

            modelBuilder.Entity<Certification>(entity =>
            {
                entity.HasKey(e => e.IdCertification);

                entity.Property(e => e.IdCertification).HasColumnName("id_certification");

                entity.Property(e => e.CodeCertifinfo)
                    .HasColumnName("code_CERTIFINFO")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.CodeRncp)
                    .HasColumnName("code_RNCP")
                    .HasMaxLength(6)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodeFormacode>(entity =>
            {
                entity.HasKey(e => e.IdCodeFormacode);

                entity.ToTable("CodeFORMACODE");

                entity.Property(e => e.IdCodeFormacode).HasColumnName("id_code_FORMACODE");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodeNsf>(entity =>
            {
                entity.HasKey(e => e.IdCodeNsf);

                entity.ToTable("CodeNSF");

                entity.Property(e => e.IdCodeNsf).HasColumnName("id_code_NSF");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(3)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CodePublicVise>(entity =>
            {
                entity.HasKey(e => e.IdCodePublicVise);

                entity.Property(e => e.IdCodePublicVise).HasColumnName("id_code_public_vise");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.CodePublicVise)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_code_public_vise_id_action");
            });

            modelBuilder.Entity<CodeRome>(entity =>
            {
                entity.HasKey(e => e.IdCodeRome);

                entity.ToTable("CodeROME");

                entity.Property(e => e.IdCodeRome).HasColumnName("id_code_ROME");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContactFormateur>(entity =>
            {
                entity.HasKey(e => e.IdContactFormateur);

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactFormateur)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_formateur");
            });

            modelBuilder.Entity<ContactFormation>(entity =>
            {
                entity.HasKey(e => e.IdContactFormation);

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactFormation)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_formation");
            });

            modelBuilder.Entity<ContactOrganisme>(entity =>
            {
                entity.HasKey(e => e.IdContactOrganisme);

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.ContactOrganisme)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_contact_organisme");
            });

            modelBuilder.Entity<ContactTel>(entity =>
            {
                entity.HasKey(e => e.IdContactTel);

                entity.Property(e => e.IdContactTel).HasColumnName("id_contact_tel");

                entity.Property(e => e.IdNumtel).HasColumnName("id_numtel");

                entity.HasOne(d => d.IdNumtelNavigation)
                    .WithMany(p => p.ContactTel)
                    .HasForeignKey(d => d.IdNumtel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_numtel_contact_tel");
            });

            modelBuilder.Entity<Coordonnee>(entity =>
            {
                entity.HasKey(e => e.IdCoordonnee);

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.Property(e => e.Civilite)
                    .HasColumnName("civilite")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Courriel)
                    .HasColumnName("courriel")
                    .HasMaxLength(160)
                    .IsUnicode(false);

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.IdFax).HasColumnName("id_fax");

                entity.Property(e => e.IdPortable).HasColumnName("id_portable");

                entity.Property(e => e.IdTelfixe).HasColumnName("id_telfixe");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");

                entity.Property(e => e.Nom)
                    .HasColumnName("nom")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prenom)
                    .HasColumnName("prenom")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.Coordonnee)
                    .HasForeignKey(d => d.IdAdresse)
                    .HasConstraintName("fk_id_adresse_coordonnee");

                entity.HasOne(d => d.IdFaxNavigation)
                    .WithMany(p => p.CoordonneeIdFaxNavigation)
                    .HasForeignKey(d => d.IdFax)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_fax_coordonnee");

                entity.HasOne(d => d.IdPortableNavigation)
                    .WithMany(p => p.CoordonneeIdPortableNavigation)
                    .HasForeignKey(d => d.IdPortable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_portable_coordonnee");

                entity.HasOne(d => d.IdTelfixeNavigation)
                    .WithMany(p => p.CoordonneeIdTelfixeNavigation)
                    .HasForeignKey(d => d.IdTelfixe)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_telfixe_coordonnee");

                entity.HasOne(d => d.IdWebNavigation)
                    .WithMany(p => p.Coordonnee)
                    .HasForeignKey(d => d.IdWeb)
                    .HasConstraintName("fk_id_web_coordonnee");
            });

            modelBuilder.Entity<CoordonneeOrganisme>(entity =>
            {
                entity.HasKey(e => e.IdCoordonneeOrganisme);

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.CoordonneeOrganisme)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_coordonnee_organisme_coordonne");
            });

            modelBuilder.Entity<DateInformation>(entity =>
            {
                entity.HasKey(e => e.IdDateInformation);

                entity.Property(e => e.IdDateInformation).HasColumnName("id_date_information");

                entity.Property(e => e.DateInformation1)
                    .HasColumnName("date_information")
                    .HasColumnType("date");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.DateInformation)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_date_information_id_action");
            });

            modelBuilder.Entity<DomaineFormation>(entity =>
            {
                entity.HasKey(e => e.IdDomaineFormation);

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");
            });

            modelBuilder.Entity<EtatRecrutement>(entity =>
            {
                entity.HasKey(e => e.IdEtatRecrutement);

                entity.Property(e => e.IdEtatRecrutement).HasColumnName("id_etat_recrutement");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecrutementKey).HasColumnName("recrutement_key");
            });

            modelBuilder.Entity<Extra>(entity =>
            {
                entity.HasKey(e => e.IdExtra);

                entity.Property(e => e.IdExtra).HasColumnName("id_extra");

                entity.Property(e => e.Commentaire)
                    .IsRequired()
                    .HasColumnName("commentaire")
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdAdresse).HasColumnName("id_adresse");

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdCertification).HasColumnName("id_certification");

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdDateInformation).HasColumnName("id_date_information");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.IdLheo).HasColumnName("id_lheo");

                entity.Property(e => e.IdModule).HasColumnName("id_module");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");

                entity.Property(e => e.IdWeb).HasColumnName("id_web");

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAction)
                    .HasConstraintName("fk_id_action_extra");

                entity.HasOne(d => d.IdAdresseNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAdresse)
                    .HasConstraintName("fk_id_adresse_extra");

                entity.HasOne(d => d.IdAdresseInscriptionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdAdresseInscription)
                    .HasConstraintName("fk_id_adresse_inscription_extra");

                entity.HasOne(d => d.IdCertificationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCertification)
                    .HasConstraintName("fk_id_certification_extra");

                entity.HasOne(d => d.IdContactFormateurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactFormateur)
                    .HasConstraintName("fk_id_contact_formateur_extra");

                entity.HasOne(d => d.IdContactFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactFormation)
                    .HasConstraintName("fk_id_contact_formation_extra");

                entity.HasOne(d => d.IdContactOrganismeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdContactOrganisme)
                    .HasConstraintName("fk_id_contact_organisme_extra");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .HasConstraintName("fk_id_coordonnee_extra");

                entity.HasOne(d => d.IdCoordonneeOrganismeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdCoordonneeOrganisme)
                    .HasConstraintName("fk_id_coordonnee_organisme_extra");

                entity.HasOne(d => d.IdDateInformationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdDateInformation)
                    .HasConstraintName("fk_id_date_information_extra");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .HasConstraintName("fk_id_domaine_formation_extra");

                entity.HasOne(d => d.IdFormationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdFormation)
                    .HasConstraintName("fk_id_formation_extra");

                entity.HasOne(d => d.IdGeolocalisationNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdGeolocalisation)
                    .HasConstraintName("fk_id_geolocalisation_extra");

                entity.HasOne(d => d.IdLheoNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdLheo)
                    .HasConstraintName("fk_id_lheo_extra");

                entity.HasOne(d => d.IdModuleNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdModule)
                    .HasConstraintName("fk_id_module_extra");

                entity.HasOne(d => d.IdModulesPrerequisNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdModulesPrerequis)
                    .HasConstraintName("fk_id_modules_prerequis_extra");

                entity.HasOne(d => d.IdOffreNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOffre)
                    .HasConstraintName("fk_id_offre_extra");

                entity.HasOne(d => d.IdOrganismeFinanceurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFinanceur)
                    .HasConstraintName("fk_id_organisme_financeur_extra");

                entity.HasOne(d => d.IdOrganismeFormateurNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFormateur)
                    .HasConstraintName("fk_id_organisme_formateur_extra");

                entity.HasOne(d => d.IdOrganismeFormationResponsableNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdOrganismeFormationResponsable)
                    .HasConstraintName("fk_id_organisme_formation_responsable_extra");

                entity.HasOne(d => d.IdPeriodeNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdPeriode)
                    .HasConstraintName("fk_id_periode_extra");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_extra");

                entity.HasOne(d => d.IdSiretNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdSiret)
                    .HasConstraintName("fk_id_SIRET_extra");

                entity.HasOne(d => d.IdSousModuleNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdSousModule)
                    .HasConstraintName("fk_id_sous_module_extra");

                entity.HasOne(d => d.IdUrlActionNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdUrlAction)
                    .HasConstraintName("fk_id_url_action_extra");

                entity.HasOne(d => d.IdWebNavigation)
                    .WithMany(p => p.Extra)
                    .HasForeignKey(d => d.IdWeb)
                    .HasConstraintName("fk_id_web_extra");
            });

            modelBuilder.Entity<Formation>(entity =>
            {
                entity.HasKey(e => e.IdFormation);

                entity.Property(e => e.IdFormation).HasColumnName("id_formation");

                entity.Property(e => e.Certifiante).HasColumnName("certifiante");

                entity.Property(e => e.ContenuFormation)
                    .IsRequired()
                    .HasColumnName("contenu_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.IdCodeNiveauSortie).HasColumnName("id_code_niveau_sortie");

                entity.Property(e => e.IdContactFormation).HasColumnName("id_contact_formation");

                entity.Property(e => e.IdDomaineFormation).HasColumnName("id_domaine_formation");

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");

                entity.Property(e => e.IdNiveauxEntree).HasColumnName("id_niveaux_entree");

                entity.Property(e => e.IdObjectifGeneralFormation).HasColumnName("id_objectif_general_formation");

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.IdParcoursDeFormation).HasColumnName("id_parcours_de_formation");

                entity.Property(e => e.IdPositionnement).HasColumnName("id_positionnement");

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");

                entity.Property(e => e.IdUrlFormation).HasColumnName("id_url_formation");

                entity.Property(e => e.IdentifiantModule)
                    .HasColumnName("identifiant_module")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.IntituleFormation)
                    .IsRequired()
                    .HasColumnName("intitule_formation")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ObjectifFormation)
                    .IsRequired()
                    .HasColumnName("objectif_formation")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.Property(e => e.ResultatsAttendus)
                    .IsRequired()
                    .HasColumnName("resultats_attendus")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdCodeNiveauSortieNavigation)
                    .WithMany(p => p.FormationIdCodeNiveauSortieNavigation)
                    .HasForeignKey(d => d.IdCodeNiveauSortie)
                    .HasConstraintName("fk_id_code_niveau_sortie_formation");

                entity.HasOne(d => d.IdContactFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdContactFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_contact_formation_formation");

                entity.HasOne(d => d.IdDomaineFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdDomaineFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_domaine_formation_formation");

                entity.HasOne(d => d.IdModulesPrerequisNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdModulesPrerequis)
                    .HasConstraintName("fk_id_modules_prerequis_formation");

                entity.HasOne(d => d.IdNiveauxEntreeNavigation)
                    .WithMany(p => p.FormationIdNiveauxEntreeNavigation)
                    .HasForeignKey(d => d.IdNiveauxEntree)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_niveaux_entree_formation");

                entity.HasOne(d => d.IdObjectifGeneralFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdObjectifGeneralFormation)
                    .HasConstraintName("fk_id_objectif_general_formation");

                entity.HasOne(d => d.IdOrganismeFormationResponsableNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdOrganismeFormationResponsable)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_organisme_formation_responsable_formation");

                entity.HasOne(d => d.IdParcoursDeFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdParcoursDeFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_parcours_de_formation_formation");

                entity.HasOne(d => d.IdPositionnementNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdPositionnement)
                    .HasConstraintName("fk_id_positionnement_formation");

                entity.HasOne(d => d.IdSousModuleNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdSousModule)
                    .HasConstraintName("fk_id_sous_module_formation");

                entity.HasOne(d => d.IdUrlFormationNavigation)
                    .WithMany(p => p.Formation)
                    .HasForeignKey(d => d.IdUrlFormation)
                    .HasConstraintName("fk_id_url_formation_formation");
            });

            modelBuilder.Entity<Geolocalisation>(entity =>
            {
                entity.HasKey(e => e.IdGeolocalisation);

                entity.Property(e => e.IdGeolocalisation).HasColumnName("id_geolocalisation");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasColumnName("latitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasColumnName("longitude")
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Lheo>(entity =>
            {
                entity.HasKey(e => e.IdLheo);

                entity.Property(e => e.IdLheo).HasColumnName("id_lheo");

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");

                entity.HasOne(d => d.IdOffreNavigation)
                    .WithMany(p => p.Lheo)
                    .HasForeignKey(d => d.IdOffre)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_offre_lheo");
            });

            modelBuilder.Entity<LieuDeFormation>(entity =>
            {
                entity.HasKey(e => e.IdLieuDeFormation);

                entity.Property(e => e.IdLieuDeFormation).HasColumnName("id_lieu_de_formation");

                entity.Property(e => e.IdCoordonnee).HasColumnName("id_coordonnee");

                entity.HasOne(d => d.IdCoordonneeNavigation)
                    .WithMany(p => p.LieuDeFormation)
                    .HasForeignKey(d => d.IdCoordonnee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_lieu_de_formation");
            });

            modelBuilder.Entity<Ligne>(entity =>
            {
                entity.HasKey(e => e.IdLigne);

                entity.Property(e => e.IdLigne).HasColumnName("id_ligne");

                entity.Property(e => e.Ligne1)
                    .IsRequired()
                    .HasColumnName("ligne")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ModaliteEnseignement>(entity =>
            {
                entity.HasKey(e => e.IdModaliteEnseignement);

                entity.Property(e => e.IdModaliteEnseignement).HasColumnName("id_modalite_enseignement");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ModaliteKey).HasColumnName("modalite_key");
            });

            modelBuilder.Entity<ModalitePedagogique>(entity =>
            {
                entity.HasKey(e => e.IdModalitePedagogique);

                entity.Property(e => e.IdModalitePedagogique).HasColumnName("id_modalite_pedagogique");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.ModalitePedagogique)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_action_modalite_information");
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.HasKey(e => e.IdModule);

                entity.Property(e => e.IdModule).HasColumnName("id_module");

                entity.Property(e => e.IdTypeModule).HasColumnName("id_type_module");

                entity.Property(e => e.ReferenceModule)
                    .IsRequired()
                    .HasColumnName("reference_module")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdTypeModuleNavigation)
                    .WithMany(p => p.Module)
                    .HasForeignKey(d => d.IdTypeModule)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_type_module_module");
            });

            modelBuilder.Entity<ModulePrerequis>(entity =>
            {
                entity.HasKey(e => e.IdModulesPrerequis);

                entity.Property(e => e.IdModulesPrerequis).HasColumnName("id_modules_prerequis");
            });

            modelBuilder.Entity<Niveaux>(entity =>
            {
                entity.HasKey(e => e.IdNiveaux);

                entity.Property(e => e.IdNiveaux).HasColumnName("id_niveaux");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(65)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Numtel>(entity =>
            {
                entity.HasKey(e => e.IdNumtel);

                entity.Property(e => e.IdNumtel).HasColumnName("id_numtel");

                entity.Property(e => e.Numero)
                    .IsRequired()
                    .HasColumnName("numero")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ObjectifGeneralFormation>(entity =>
            {
                entity.HasKey(e => e.IdObjectifGeneralFormation);

                entity.Property(e => e.IdObjectifGeneralFormation).HasColumnName("id_objectif_general_formation");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(70)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Offre>(entity =>
            {
                entity.HasKey(e => e.IdOffre);

                entity.Property(e => e.IdOffre).HasColumnName("id_offre");
            });

            modelBuilder.Entity<OrganismeFinanceur>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFinanceur);

                entity.Property(e => e.IdOrganismeFinanceur).HasColumnName("id_organisme_financeur");

                entity.Property(e => e.CodeFinanceur).HasColumnName("code_financeur");

                entity.Property(e => e.NbPlaceFinancees).HasColumnName("nb_place_financees");
            });

            modelBuilder.Entity<OrganismeFormateur>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFormateur);

                entity.Property(e => e.IdOrganismeFormateur).HasColumnName("id_organisme_formateur");

                entity.Property(e => e.IdContactFormateur).HasColumnName("id_contact_formateur");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiretFormation).HasColumnName("id_siret_formation");

                entity.Property(e => e.RaisonSocialeFormateur)
                    .IsRequired()
                    .HasColumnName("raison_sociale_formateur")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdContactFormateurNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdContactFormateur)
                    .HasConstraintName("fk_id_contact_formateur_organisme_formateur");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_organisme_formateur");

                entity.HasOne(d => d.IdSiretFormationNavigation)
                    .WithMany(p => p.OrganismeFormateur)
                    .HasForeignKey(d => d.IdSiretFormation)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_organisme_formateur");
            });

            modelBuilder.Entity<OrganismeFormationResponsable>(entity =>
            {
                entity.HasKey(e => e.IdOrganismeFormationResponsable);

                entity.Property(e => e.IdOrganismeFormationResponsable).HasColumnName("id_organisme_formation_responsable");

                entity.Property(e => e.IdContactOrganisme).HasColumnName("id_contact_organisme");

                entity.Property(e => e.IdCoordonneeOrganisme).HasColumnName("id_coordonnee_organisme");

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.NomOrganisme)
                    .IsRequired()
                    .HasColumnName("nom_organisme")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroActivite)
                    .IsRequired()
                    .HasColumnName("numero_activite")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.RaisonSociale)
                    .IsRequired()
                    .HasColumnName("raison_sociale")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.RenseignementsSpecifiques)
                    .HasColumnName("renseignements_specifiques")
                    .HasMaxLength(3000)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdContactOrganismeNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdContactOrganisme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_contact_organisme_organisme_formation_responsable");

                entity.HasOne(d => d.IdCoordonneeOrganismeNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdCoordonneeOrganisme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_coordonnee_organisme_organisme_formation_responsable");

                entity.HasOne(d => d.IdPotentielNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdPotentiel)
                    .HasConstraintName("fk_id_potentiel_organisme_formation_responsable");

                entity.HasOne(d => d.IdSiretNavigation)
                    .WithMany(p => p.OrganismeFormationResponsable)
                    .HasForeignKey(d => d.IdSiret)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_SIRET_organisme_formation_responsable");
            });

            modelBuilder.Entity<ParcoursDeFormation>(entity =>
            {
                entity.HasKey(e => e.IdParcoursDeFormation);

                entity.Property(e => e.IdParcoursDeFormation).HasColumnName("id_parcours_de_formation");

                entity.Property(e => e.Libele)
                    .IsRequired()
                    .HasColumnName("libele")
                    .HasMaxLength(35)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PerimetreRecrutement>(entity =>
            {
                entity.HasKey(e => e.IdPerimetreRecrutement);

                entity.HasIndex(e => e.Cle)
                    .HasName("UK_perimetre_recrutement_cle")
                    .IsUnique();

                entity.Property(e => e.IdPerimetreRecrutement).HasColumnName("id_perimetre_recrutement");

                entity.Property(e => e.Cle).HasColumnName("cle");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasColumnName("label")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Periode>(entity =>
            {
                entity.HasKey(e => e.IdPeriode);

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.Debut)
                    .HasColumnName("debut")
                    .HasColumnType("date");

                entity.Property(e => e.Fin)
                    .HasColumnName("fin")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Positionnement>(entity =>
            {
                entity.HasKey(e => e.IdPositionnement);

                entity.Property(e => e.IdPositionnement).HasColumnName("id_positionnement");

                entity.Property(e => e.Libele)
                    .HasColumnName("libele")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Potentiel>(entity =>
            {
                entity.HasKey(e => e.IdPotentiel);

                entity.Property(e => e.IdPotentiel).HasColumnName("id_potentiel");
            });

            modelBuilder.Entity<ReferenceModule>(entity =>
            {
                entity.HasKey(e => e.IdReferenceModule);

                entity.Property(e => e.IdReferenceModule).HasColumnName("id_reference_module");

                entity.Property(e => e.Reference)
                    .IsRequired()
                    .HasColumnName("reference")
                    .HasMaxLength(3000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Session>(entity =>
            {
                entity.HasKey(e => e.IdSession);

                entity.ToTable("_Session");

                entity.Property(e => e.IdSession).HasColumnName("id_session");

                entity.Property(e => e.IdAction).HasColumnName("id_action");

                entity.Property(e => e.IdAdresseInscription).HasColumnName("id_adresse_inscription");

                entity.Property(e => e.IdEtatRecrutement).HasColumnName("id_etat_recrutement");

                entity.Property(e => e.IdPeriode).HasColumnName("id_periode");

                entity.Property(e => e.IdPeriodeInscription).HasColumnName("id_periode_inscription");

                entity.Property(e => e.ModaliteInscription)
                    .IsRequired()
                    .HasColumnName("modalite_inscription")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdActionNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdAction)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_action_session");

                entity.HasOne(d => d.IdAdresseInscriptionNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdAdresseInscription)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_adresse_inscription_session");

                entity.HasOne(d => d.IdEtatRecrutementNavigation)
                    .WithMany(p => p.Session)
                    .HasForeignKey(d => d.IdEtatRecrutement)
                    .HasConstraintName("fk_id_etat_recrutement_session");

                entity.HasOne(d => d.IdPeriodeNavigation)
                    .WithMany(p => p.SessionIdPeriodeNavigation)
                    .HasForeignKey(d => d.IdPeriode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_id_periode_session");

                entity.HasOne(d => d.IdPeriodeInscriptionNavigation)
                    .WithMany(p => p.SessionIdPeriodeInscriptionNavigation)
                    .HasForeignKey(d => d.IdPeriodeInscription)
                    .HasConstraintName("fk_id_periode_inscription_session");
            });

            modelBuilder.Entity<Siret>(entity =>
            {
                entity.HasKey(e => e.IdSiret);

                entity.ToTable("SIRET");

                entity.Property(e => e.IdSiret).HasColumnName("id_SIRET");

                entity.Property(e => e.Siret1)
                    .IsRequired()
                    .HasColumnName("SIRET")
                    .HasMaxLength(14)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SousModule>(entity =>
            {
                entity.HasKey(e => e.IdSousModule);

                entity.Property(e => e.IdSousModule).HasColumnName("id_sous_module");
            });

            modelBuilder.Entity<TypeModule>(entity =>
            {
                entity.HasKey(e => e.IdTypeModule);

                entity.Property(e => e.IdTypeModule).HasColumnName("id_type_module");

                entity.Property(e => e.Libele)
                    .HasColumnName("libele")
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UrlAction>(entity =>
            {
                entity.HasKey(e => e.IdUrlAction);

                entity.Property(e => e.IdUrlAction).HasColumnName("id_url_action");
            });

            modelBuilder.Entity<UrlFormation>(entity =>
            {
                entity.HasKey(e => e.IdUrlFormation);

                entity.Property(e => e.IdUrlFormation).HasColumnName("id_url_formation");
            });

            modelBuilder.Entity<UrlWeb>(entity =>
            {
                entity.HasKey(e => e.IdUrlWeb);

                entity.Property(e => e.IdUrlWeb).HasColumnName("id_url_web");

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasColumnName("url")
                    .HasMaxLength(400)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Web>(entity =>
            {
                entity.HasKey(e => e.IdWeb);

                entity.Property(e => e.IdWeb).HasColumnName("id_web");
            });
            modelBuilder.Entity<User>().HasIndex(u => u.Email)
                .IsUnique();
            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(ur => new { ur.IdRole, ur.IdUser });
                entity.HasOne(ur => ur.User)
                    .WithMany(u => u.UserRoles)
                    .HasForeignKey(ur => ur.IdUser);
                entity.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.IdRole);
            });
        }
    }
}
