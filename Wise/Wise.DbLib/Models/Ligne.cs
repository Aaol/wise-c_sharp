﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Ligne
    {
        public int IdLigne { get; set; }
        public string Ligne1 { get; set; }
    }
}
