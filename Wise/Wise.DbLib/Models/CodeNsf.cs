﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeNsf
    {
        public int IdCodeNsf { get; set; }
        public string Code { get; set; }
    }
}
