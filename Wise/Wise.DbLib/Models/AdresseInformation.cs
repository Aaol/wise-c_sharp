﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class AdresseInformation
    {
        public AdresseInformation()
        {
            Action = new HashSet<Action>();
        }

        public int IdAdresseInformation { get; set; }
        public int IdAdresse { get; set; }

        public Adresse IdAdresseNavigation { get; set; }
        public ICollection<Action> Action { get; set; }
    }
}
