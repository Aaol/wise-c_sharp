﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Periode
    {
        public Periode()
        {
            Extra = new HashSet<Extra>();
            SessionIdPeriodeInscriptionNavigation = new HashSet<Session>();
            SessionIdPeriodeNavigation = new HashSet<Session>();
        }

        public int IdPeriode { get; set; }
        public DateTime Debut { get; set; }
        public DateTime Fin { get; set; }

        public ICollection<Extra> Extra { get; set; }
        public ICollection<Session> SessionIdPeriodeInscriptionNavigation { get; set; }
        public ICollection<Session> SessionIdPeriodeNavigation { get; set; }
    }
}
