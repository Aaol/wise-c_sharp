﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodePublicVise
    {
        public int IdCodePublicVise { get; set; }
        public int IdAction { get; set; }
        public string Code { get; set; }

        public Action IdActionNavigation { get; set; }
    }
}
