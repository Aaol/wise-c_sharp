﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class CodeFormacode
    {
        public int IdCodeFormacode { get; set; }
        public string Code { get; set; }
    }
}
