﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wise.DbLib.Models
{
    /// <summary>
    ///     Classe définissant un utilisateur
    /// </summary>
    public class User
    {
        #region Properties

        /// <summary>
        ///     Identifiant de l'utilisateur
        /// </summary>
        [Key]
        public long IdUser { get; set; }
        
        /// <summary>
        ///     Address Email de l'utilisateur
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Hash du mot de passe de l'utilisateur
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        ///     Liste des rôles de l'utilisateur
        /// </summary>
        public List<UserRole> UserRoles { get; set; }

        /// <summary>
        ///     Sel de l'utilisateur utilisé pour hasher le mot de passe.
        /// </summary>
        public string Salt { get; set; }

        #endregion
    }
}
