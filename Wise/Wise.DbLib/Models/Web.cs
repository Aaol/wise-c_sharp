﻿using System;
using System.Collections.Generic;

namespace Wise.DbLib.Models
{
    public partial class Web
    {
        public Web()
        {
            Action = new HashSet<Action>();
            Coordonnee = new HashSet<Coordonnee>();
            Extra = new HashSet<Extra>();
        }

        public int IdWeb { get; set; }

        public ICollection<Action> Action { get; set; }
        public ICollection<Coordonnee> Coordonnee { get; set; }
        public ICollection<Extra> Extra { get; set; }
    }
}
