﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Wise.API.Config;
using Wise.DbLib.Models;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Contrôleur pour les utilisateurs
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        #region Properties

        /// <summary>
        ///     Utilisé pour la génération des utilisateurs
        /// </summary>
        public RNGCryptoServiceProvider RNGCryptoService { get; set; }

        public IOptions<JwtKeyConfig> JwtKeyConfig { get; set; }

        #endregion

        #region Constructors
        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="UserController"/>.
        /// </summary>
        /// <param name="context"></param>
        public UserController(WiseNetworkContext context, IOptions<JwtKeyConfig> jwtKeyConfig) : base(context)
        {
            this.RNGCryptoService = new RNGCryptoServiceProvider();
            this.JwtKeyConfig = jwtKeyConfig;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Créé un nouveau <see cref="User"/>.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult CreateUser([FromBody] UserCreation user)
        {
            if (user.ConfirmPassword != user.Password)
            {
                return StatusCode(400, "Le mot de passe et la confirmation du mot de passe ne correspondent pas.");
            }
            // Création du nouvel utilisateur
            User newUser = new User()
            {
                Email = user.Email,
            };

            // Voir https://medium.com/@mehanix/lets-talk-security-salted-password-hashing-in-c-5460be5c3aae pour le hashage du mot de passe

            // Création du sel pour le hashage.
            byte[] salt;
            this.RNGCryptoService.GetBytes(salt = new byte[16]);

            Rfc2898DeriveBytes pdfkf2 = new Rfc2898DeriveBytes(user.Password, salt, 10000);

            byte[] hash = pdfkf2.GetBytes(20);

            byte[] hashBytes = new byte[36];

            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string passwordHash = Convert.ToBase64String(hashBytes);
            string saltString = Convert.ToBase64String(salt);

            try
            {
                // On vient sauvegarder l'utilisateur courant.
                newUser.Password = passwordHash;
                newUser.Salt = saltString;
                this.Entities.Users.Add(newUser);

                this.Entities.SaveChanges();
            }
            catch (Exception)
            {
                return StatusCode(500, "Une erreur est survenue pendant l'ajout de l'utilisateur");
            }
            return StatusCode(201);
        }

        /// <summary>
        ///     Connexion de l'utilisateur
        /// </summary>
        /// <param name="login">Données de connexion de l'utilisateurs</param>
        /// <returns>Renvoie le token JWT</returns>
        [HttpPost("login")]
        public IActionResult Login([FromBody] UserLogin login)
        {
            User user;
            // Récupération de l'utilisateur en fonction de l'
            try
            {
                user = this.Entities.Users
                   .Include(u => u.UserRoles)
                   .ThenInclude(ur => ur.Role)
                   .First(u => u.Email == login.Email);

            }
            catch (Exception)
            {
                return StatusCode(404);
            }

            try
            {
                byte[] hashBytes = Convert.FromBase64String(user.Password);

                byte[] salt = new byte[16];
                Array.Copy(hashBytes, 0, salt, 0, 16);
                Rfc2898DeriveBytes pdfkf2 = new Rfc2898DeriveBytes(login.Password, salt, 10000);

                byte[] hash = pdfkf2.GetBytes(20);


                for(int i = 0; i < 20; i++)
                {
                    if(hashBytes[i + 16] != hash[i])
                    {
                        return StatusCode(404);
                    }
                }
            }
            catch (Exception)
            {
                return StatusCode(500);
            }

            List<Claim> claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.Email)
                };

            user.UserRoles.ToList().ForEach(role =>
            {
                claims.Add(new Claim(ClaimTypes.Role, role.Role.RoleName));
            });

            DateTime issueDate = DateTime.Now;
            DateTime expireDate = issueDate.AddHours(2);
            //On affecte la date d'expiration du token et du refreshtoken en fonction du rôle de l'utilisateur
            
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtKeyConfig.Value.JwtKey));

             JwtSecurityToken token = new JwtSecurityToken(new JwtHeader(new SigningCredentials(key, SecurityAlgorithms.HmacSha256))
                , new JwtPayload("WiseNetwork", "The name of the audience", claims, issueDate, expireDate));
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }

        #endregion
    }
}
