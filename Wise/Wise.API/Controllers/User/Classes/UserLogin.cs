﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe proxy utilisée pour la connexion d'un utilisateur
    /// </summary>
    public class UserLogin
    {
        #region Properties

        /// <summary>
        ///     Emil de connexion de l'utilisateur.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        ///     Mot de passe de connexion de l'utilisateur.
        /// </summary>
        public string Password { get; set; }

        #endregion
    }
}
