﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wise.DbLib.Models;

namespace Wise.API.Controllers
{
    /// <summary>
    ///     Classe de base pour les controleurs d'API Rest
    /// </summary>
    public abstract class BaseController : ControllerBase
    {
        #region Properties

        /// <summary>
        ///     Contexte d'accès aux données
        /// </summary>
        protected WiseNetworkContext Entities { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initialise une nouvelle instance de la classe <see cref="BaseController"/>.
        /// </summary>
        public BaseController(WiseNetworkContext context)
        {
            this.Entities = context;
        }

        #endregion
    }
}
