﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Repositories
{
    public interface IBaseRepository<T>
    {
        T GetEntity(int id);
        IQueryable<T> GetEntities();
        void DeleteEntity(int id);
        T AddEntity(T entity);
        T updateEntity(T entity);
    }
}
