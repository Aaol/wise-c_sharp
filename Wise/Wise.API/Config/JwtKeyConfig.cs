﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wise.API.Config
{
    /// <summary>
    ///     Classe utilisée pour la configuration des jetons d'accès JWT
    /// </summary>
    public class JwtKeyConfig
    {
        public string JwtKey { get; set; }
    }
}
